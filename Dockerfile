FROM debian:10.7

RUN dpkg --add-architecture i386
RUN apt-get update 
RUN apt-get install -y git
RUN apt-get install -y  g++-8 g++-multilib make cmake valgrind libc6-dbg:i386
RUN apt-get install -y python3 python3-pip
RUN pip3 install conan

ENTRYPOINT bash
